import logging
import pprint

from django.views.generic import TemplateView

logger = logging.getLogger(__name__)


class HelloTemplateView(TemplateView):
    """Simple Hello World Template View"""

    template_name = "directory/hello.html"

    def get_context_data(self, **kwargs):
        """Get the rendered context data."""
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Hello"
        context["page_description"] = "Hello page !"
        context["name"] = self.request.GET.get("name", "World")
        logger.debug(f"{self.__class__.__name__}::get_context_data() - context: {pprint.pformat(context)}")
        return context
