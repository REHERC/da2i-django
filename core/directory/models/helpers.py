"""Helpers for models on :mod:`directory`."""

import logging

from django.apps import apps
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.db import models
from django_currentuser.middleware import get_current_authenticated_user

logger = logging.getLogger(__name__)


class Administrable(models.Model):
    """An administrable model takes cares of when and who created and updated it."""

    #: Creation date as a :class:`python:datetime.datetime`
    created_at = models.DateTimeField(
        verbose_name=_("created at"),
        auto_now_add=True,
        editable=False,
    )

    #: Creator as a :class:`django:django.contrib.auth.models.User`
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("created by"),
        related_name="%(app_label)s_%(class)s_owned",  # noqa
        default=1,
        on_delete=models.SET_DEFAULT,
        editable=False,
    )

    #: Update date as a :class:`python:datetime.datetime`
    updated_at = models.DateTimeField(
        verbose_name=_("created at"),
        auto_now=True,
        editable=False,
        null=True,
    )

    #: Updater as a :class:`django:django.contrib.auth.models.User`
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("created by"),
        related_name="%(app_label)s_%(class)s_updated",  # noqa
        null=True,
        on_delete=models.SET_NULL,
        editable=False,
    )

    class Meta:
        """Meta class."""

        abstract = True

    def save_base(
        self,
        raw=False,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
    ):
        """
        Get the creator and  updater from the current thread and save it.

        .. note::
           Into the into:

           + :attr:`eventrequest.models.helpers.Administrable.created_by`
           + :attr:`eventrequest.models.helpers.Administrable.updated_by`

        :param raw: Raw SQL query ?
        :param force_insert: Force insertion
        :param force_update: Force update
        :param using:  DB alias used
        :param update_fields: List fields to update
        :return: Nothing
        """
        current_user = get_current_authenticated_user()
        default_user = apps.get_model(settings.AUTH_USER_MODEL).objects.get(pk=1)

        if current_user is None:
            current_user = default_user
            logger.warning(
                f"{self.__class__.__name__}::save_base() "
                f'Unable to get the current user from local thread: setting the default one: "{current_user}"'
            )

        # Looks like an instance creation
        if self.updated_by is None and self.created_by == default_user:
            self.created_by = self.updated_by = current_user
            logging.info(
                f"{self.__class__.__name__}::save_base() "
                f"Creates a new instance ({self}): created_by = {self.created_by}"
            )
        else:
            self.updated_by = current_user
            logging.info(
                f"{self.__class__.__name__}::save_base() "
                f"Updates an existing instance ({self}): updated_by = {self.updated_by}"
            )

        super(Administrable, self).save_base(
            raw=raw,
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
        )
