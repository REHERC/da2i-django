django-debug-toolbar==3.2.4
tox==3.24.5
pytest==7.0.1
pytest-django==4.5.2
pygraphviz~=1.9
