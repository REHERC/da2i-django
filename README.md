# DA2I - 2023
Projet d'études pour les étudiants de la Licence Professionnelle DA2I - IUT de Lille A, promotion 21/22.

## Présentation
L'IUT a besoin de créer un annuaire des étudiants, incluant leur classe et leurs options.

Il s'agit de réaliser un annuaire des étudiants sous forme d'application Web, développée avec [Django](https://www.djangoproject.com/).

## Attendus

### Back-office

+ Administration des classes
+ Administration des options
+ Administration des étudiants

En option, il sera possible d'assigner des binômes aux étudiants.

### Front-office

+ Recherche des étudiants
+ Affichage des groupes de classes
+ Affichage des options et des étudiants qui y sont inscrits
+ Affichage des étudiants

Les models de données ont été réalisés, ainsi que les bases du Back-office.

## Utilisation
### Installation des sources mises à jour
```bash
mkdir DA2I
cd DA2I
git clone https://gitlab.com/frague59/da2i_2023.git
cd da2i_2023
virtualenv -p python3 venv
. venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
cd core
python manage.py migrate
python manage.py runserver
```

### Reste à faire

+ [ ] Ajout d'un binôme
+ [ ] Recherche des étudiants
+ [ ] Affichage des groupes de classes
+ [ ] Affichage des options et des étudiants qui y sont inscrits
+ [ ] Affichage des étudiants
